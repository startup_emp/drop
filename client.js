import express from 'express'
import { createConnection } from 'mysql'
import { urlencoded, json } from 'body-parser'
import { writeFileSync } from 'fs'
// const routes = require('routes');

// logging

import { logger } from './route/common/logger'

// For Allowing Cross Origin Requests
import cors from 'cors'

// var key = require('./constants').secretkey
import { host, db, username, password, multipleStatements } from './constants'

// Routes
import firstPage from './route/client/first-page'
import loginPage from './route/client/login-page'
import newUser from './route/client/newuser'
import addressPage from './route/client/address-page'
import orderPage from './route/client/order-page'

// Not using
/* import aes from 'crypto-js/aes'
import CryptoJS from 'crypto-js'
import sha from 'crypto-js/sha256'
import async from 'async' */
import { randomBytes, createHmac } from 'crypto'
// ------------------------- Require Ends -------------------------------
var app = express()
// ------------------------- Middleware Starts --------------------------
app.use(cors()) // For Allowing Cross Origin Requests
// For getting body input in URL encoded format
app.use(urlencoded({
  extended: true
}))
app.use(json())

// ------------------------- Middleware Ends --------------------------

// ------------------------- Initialization Code Starts -----------------------
var phone = null

// Create connection to mysql db
var connection = createConnection({
  host: host,
  database: db,
  user: username,
  password: password,
  multipleStatements: multipleStatements
})

// Run this function everyday to update pincode.json from db
function updatePincode () {
  console.log('This pops up every 5 seconds and is annoying!')
  connection.query('select * from drop.pincode_details', function (err, rows, fields) {
    if (err) {
      logger.error(err)
      throw err
    }
    writeFileSync('./Res/pincode.json', JSON.stringify(rows), 'utf-8')
  })
}
// ------------------------- Initialization Code Ends -----------------------

// routes
//  Connect all our routes to our application
app.use('/', firstPage)
app.use('/', loginPage)
app.use('/', newUser)
app.use('/', addressPage)
app.use('/', orderPage)
/* require('./code/client/first-page')(app, connection);
require('./code/client/newuser')(app, connection);
require('./code/client/login-page')(app, connection); */

/* app.use(
  expressjwt({
    secret: key
  }),
  function (req, res, next) {

    logger.info('Hello world');
    logger.warn('Warning message');
    logger.debug('Debugging info');
    console.log("R" + req.user.userid);
    phone = req.user.userid;
    next();
  }
); */

/* app.use(
  jwt.verify(token, key, function (err, decoded, next) {
    console.log(decoded.userid);
    next();
  })
); */ // Middleware for Decrypting JWT to user object

app.post('/orderhistory', function (req, res) {
  var phone = req.body.phoneno
  let sql = `CALL USP_Order_History(?)`

  connection.query(sql, phone, (error, results, fields) => {
    if (error) {
      return console.error(error.message)
    }
    console.log(results[0])
  })
})

// ------------Order confirmation page---------------

// ------------Order confirmation Page Ends---------------

// ------------Catch wrong Routes starts---------------
app.all('*', function (req, res) {
  console.log('wrong route')
})
// ------------Catch wrong Routes Ends---------------

var phoneandpassword = function (phoneno) {
  var phone
  connection.query('select phone,password from drop.user where phone = ?', phoneno, function (err, rows, fields) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
      }
      logger.error(err)
      res.send(result)
      throw err
    }
    phone = rows[0].password
    // console.log(rows[0].password + " " +phone);
    // console.log(phone + 1);
    /* connection.query('insert into drop.user values (?,?)', [phone + 1, password], function (err) {
      if (err) throw err
    }); */
  })
  return phone
}

/* app.get('/:blocks', function (req, res) {
  var phone, phoneno = req.params.blocks;

  phoneandpassword(phoneno);
  async.series([
    phoneandpassword(phoneno)
  ]).call().end(function (err, result) {
    phone = result;
    console.log(phone);
  });
  console.log(phone);
}); */

/* var encrypt = function (plain) {
  //Encryption and Decryption start
  console.log(pass + "\n\nEncrypt:\n");
  var enc = aes.encrypt(pass, "elan");
  console.log(enc + "\n\nDecrypt:\n");
  var dec = aes.decrypt(enc, "elan").toString(CryptoJS.enc.Utf8);
  console.log(dec + "\n\nPass:");
  //Encryption and Decryption ends

  //One Way Hashing starts
  console.log(pass + "\n\nEncrypt:\n");
  var enc = sha(pass).toString();;
  console.log(enc + "\n");
  var dec = sha(pass).toString();;
  console.log(dec);
  //One Way Hashing ends
}; */

var genRandomString = function (length) {
  return randomBytes(Math.ceil(length / 2))
    .toString('hex') /** convert to hexadecimal format */
    .slice(0, length) /** return required number of characters */
}

app.listen(8099, function () {
  // logging();

  // setInterval(updatePincode, 5000);
  // geoCode();
  console.log('Listening now in 8099')
})

var geoCode = function () {
  var googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyAu4yAiXkcs28yrPMl_iIkW61FfAQkU4DE'
  })

  // Geocode an address.
  googleMapsClient.geocode({
    address: 'No.2, 4th Avenue, Pallava Garden, Zamin Pallavaram, Chennai - 600117'
  }, function (err, response) {
    if (!err) {
      console.log(response.json.results)
    }
  })
}

var encrypt = function (password) {
  var salt = genRandomString(4)
  const hash = createHmac('sha1', '85ef')
  hash.update(password)
  var value = hash.digest('hex')
  return {
    salt: salt,
    passwordHash: value
  }
}
