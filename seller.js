var express = require('express');
var app = express();
var mysql = require('mysql');
var aes = require('crypto-js/aes');
var CryptoJS = require('crypto-js');
var sha = require('crypto-js/sha256');
var async = require('async');

//For parsing the body of post request

var bodyParser = require('body-parser');
//For Allowing Cross Origin Requests
var cors = require('cors')

app.use(cors())
app.use(bodyParser.json());

// var phoneno, password;

var connection = mysql.createConnection({
  host: 'localhost',
  user: 'drop',
  password: 'drop',
  database: 'drop'
});

/**Check if the Phone Number received from the client app is registered
 *Req=>/login?phoneno=7418209937
     =>/login?phoneno=7418209937&password=elan
 *Res=>err || "User not registered" || the phone number sent in the req
 */

app.get('/login', function (req, res) {
  var phone = req.query.phoneno;
  var password = req.query.password;
  if (password) {
    connection.query('select password from drop.user where phone = ?', phone, function (err, rows, fields) {
      if (err) throw err;
      if (rows.length <= 0)
        res.send("error");
      else
      if (password === rows[0].password)
        res.send(rows);
    });
  }
  else
    res.send("User not registered");
});

/**Check if the password corrosponding to the phone number matches in the db 
 *Req=>/regfullcheck?phoneno=7418209937&password=elan
 *Res=>err || "error" || the password corrosponding to the phone number || "Wrong Password"
 */
app.get('/regfullcheck', function (req, res) {
  var phone = req.query.phoneno;
  var password = req.query.password;

  console.log(phone + " " + password);
  connection.query('select password from drop.user where phone = ?', phone, function (err, rows, fields) {
    if (err) throw err;
    if (rows.length <= 0)
      res.send("error");
    else
    if (password == rows[0].password)
      res.send(rows[0].password);
    else
      res.send("Wrong Passowrd");
  });
});

app.listen(8080, function () {
  console.log("Listening");
});