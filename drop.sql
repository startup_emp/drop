# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: dropdb.ccdordd819ji.ap-southeast-1.rds.amazonaws.com (MySQL 5.6.39-log)
# Database: dropDB
# Generation Time: 2018-10-18 13:59:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table address_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `address_details`;

CREATE TABLE `address_details` (
  `address_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `door_no` varchar(45) NOT NULL,
  `street` varchar(45) NOT NULL,
  `area` varchar(45) NOT NULL,
  `town_city` varchar(45) NOT NULL,
  `pincode_id` mediumint(6) unsigned NOT NULL,
  `landmark` varchar(45) NOT NULL,
  `state` char(15) NOT NULL DEFAULT 'tamilnadu',
  PRIMARY KEY (`address_id`),
  KEY `pincode_idx` (`pincode_id`),
  KEY `area_idx` (`area`),
  CONSTRAINT `a_pincode` FOREIGN KEY (`pincode_id`) REFERENCES `pincode_details` (`pincode_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `area` FOREIGN KEY (`area`) REFERENCES `pincode_details` (`area`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `address_details` WRITE;
/*!40000 ALTER TABLE `address_details` DISABLE KEYS */;

INSERT INTO `address_details` (`address_id`, `name`, `phone`, `door_no`, `street`, `area`, `town_city`, `pincode_id`, `landmark`, `state`)
VALUES
	(1,'asf',8848,'asd','asd','mana','sdfgsd',1,'asdf','tamilnadu'),
	(2,'asdfsdf',6262,'arfr','asdf','palla','ergerg',2,'egrdg','tamilnadu'),
	(5,'rh',7656,'dgn','dfb','anna','dfbdb',3,'bdfgn','tamilnadu'),
	(6,'asf',8848,'asd','asd','mana','sdfgsd',1,'asdf','tamilnadu'),
	(7,'asf',8848,'asd','asd','mana','sdfgsd',1,'asdf','tamilnadu'),
	(8,'asf',8848,'asd','asd','mana','sdfgsd',1,'asdf','tamilnadu'),
	(9,'asf',848,'asd','asd','mana','sgsd',1,'asd','tamilnadu'),
	(11,'asf',8848,'asd','asd','mana','sdfgsd',1,'asdf','tamilnadu'),
	(12,'asf',8848,'asd','asd','mana','sdfgsd',1,'asdf','tamilnadu'),
	(13,'asf',8848,'asd','asd','mana','sdfgsd',1,'asdf','tamilnadu');

/*!40000 ALTER TABLE `address_details` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table device_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `device_details`;

CREATE TABLE `device_details` (
  `userid` mediumint(6) unsigned NOT NULL,
  `uuid` char(36) NOT NULL DEFAULT '',
  `token` text NOT NULL,
  UNIQUE KEY `Composite Key` (`uuid`,`userid`),
  KEY `device_userid` (`userid`),
  CONSTRAINT `device_userid` FOREIGN KEY (`userid`) REFERENCES `user_details` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table device_identity
# ------------------------------------------------------------

DROP TABLE IF EXISTS `device_identity`;

CREATE TABLE `device_identity` (
  `uuid` int(11) unsigned NOT NULL,
  `userid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uuid`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table firebase
# ------------------------------------------------------------

DROP TABLE IF EXISTS `firebase`;

CREATE TABLE `firebase` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table order_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `order_details`;

CREATE TABLE `order_details` (
  `order_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `phone` bigint(10) unsigned NOT NULL,
  `timeslot_id` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `seller_id` mediumint(6) unsigned DEFAULT NULL,
  `address_id` mediumint(6) unsigned NOT NULL,
  `quantity` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `user_id` mediumint(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `address_idx` (`address_id`),
  KEY `o_tiemslot_idx` (`timeslot_id`),
  KEY `o_sellert_id_idx` (`seller_id`),
  KEY `o_user_id_idx` (`user_id`),
  CONSTRAINT `o_address` FOREIGN KEY (`address_id`) REFERENCES `address_details` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `o_sellert_id` FOREIGN KEY (`seller_id`) REFERENCES `seller_details` (`s_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `o_timeslot` FOREIGN KEY (`timeslot_id`) REFERENCES `timeslots` (`timeslot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `o_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;

INSERT INTO `order_details` (`order_id`, `phone`, `timeslot_id`, `seller_id`, `address_id`, `quantity`, `user_id`)
VALUES
	(2,7418209937,2,NULL,5,3,NULL),
	(7,0,1,NULL,1,2,1),
	(8,7418209937,1,NULL,1,2,1);

/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_details`;

CREATE TABLE `password_details` (
  `password_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `password` varchar(60) NOT NULL,
  PRIMARY KEY (`password_id`),
  UNIQUE KEY `password_UNIQUE` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Password for both user and seller';

LOCK TABLES `password_details` WRITE;
/*!40000 ALTER TABLE `password_details` DISABLE KEYS */;

INSERT INTO `password_details` (`password_id`, `password`)
VALUES
	(5,'$2a$10$L7S9Knz/cHcxqUEnBsacie6FQnntw.HaXKpA4yRw2vYuZCgzon3oW'),
	(2,'$2a$10$xh2j7TRKQ0C4r6vvvhKaKeybXiWZso4NzgoQFzFDJZV..CXIOrl46');

/*!40000 ALTER TABLE `password_details` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table payment_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `payment_details`;

CREATE TABLE `payment_details` (
  `payment_id` varchar(12) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pincode_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pincode_details`;

CREATE TABLE `pincode_details` (
  `pincode_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `pincode` mediumint(9) unsigned NOT NULL,
  `area` varchar(45) NOT NULL,
  PRIMARY KEY (`pincode_id`),
  UNIQUE KEY `area_UNIQUE` (`area`),
  UNIQUE KEY `area_pin_unique` (`pincode`,`area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pincode_details` WRITE;
/*!40000 ALTER TABLE `pincode_details` DISABLE KEYS */;

INSERT INTO `pincode_details` (`pincode_id`, `pincode`, `area`)
VALUES
	(7,600001,'anna'),
	(1,600101,'chen'),
	(3,600101,'kanche'),
	(4,600101,'palla'),
	(2,600102,'madur'),
	(6,600111,'tnj'),
	(5,600117,'mana');

/*!40000 ALTER TABLE `pincode_details` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table seller_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `seller_details`;

CREATE TABLE `seller_details` (
  `s_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `s_name` varchar(30) NOT NULL,
  `s_address_id` mediumint(6) unsigned NOT NULL,
  `phone` bigint(20) unsigned NOT NULL,
  `timeslot_id` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `quantity` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `password_id` mediumint(6) unsigned NOT NULL,
  `location_lat` point DEFAULT NULL,
  `location_long` point DEFAULT NULL,
  `s_status` set('registered','available','not_available') NOT NULL DEFAULT 'registered',
  `out_f_seller` int(11) unsigned DEFAULT NULL COMMENT 'Outstanding amount from seller',
  `out_t_seller` int(11) unsigned DEFAULT NULL COMMENT 'Outstanding amount to seller',
  `today_qty` tinyint(3) unsigned DEFAULT NULL,
  `today_amt_t_seller` int(11) unsigned DEFAULT NULL,
  `today_amt_f_seller` int(10) unsigned DEFAULT NULL,
  `area` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`s_id`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  KEY `address_idx` (`s_address_id`),
  KEY `timeslot_idx` (`timeslot_id`),
  KEY `s_password_idx` (`password_id`),
  CONSTRAINT `s_address` FOREIGN KEY (`s_address_id`) REFERENCES `address_details` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `s_password` FOREIGN KEY (`password_id`) REFERENCES `password_details` (`password_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `s_timeslot` FOREIGN KEY (`timeslot_id`) REFERENCES `timeslots` (`timeslot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table timeslots
# ------------------------------------------------------------

DROP TABLE IF EXISTS `timeslots`;

CREATE TABLE `timeslots` (
  `timeslot_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `timeslot` set('8-12','2-6','6-10') NOT NULL,
  PRIMARY KEY (`timeslot_id`),
  UNIQUE KEY `timeslot_UNIQUE` (`timeslot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='For containing timeslots as the foreign key.';

LOCK TABLES `timeslots` WRITE;
/*!40000 ALTER TABLE `timeslots` DISABLE KEYS */;

INSERT INTO `timeslots` (`timeslot_id`, `timeslot`)
VALUES
	(1,'8-12'),
	(2,'2-6'),
	(3,'6-10');

/*!40000 ALTER TABLE `timeslots` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_details`;

CREATE TABLE `user_details` (
  `user_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `def_timeslot` tinyint(3) unsigned DEFAULT NULL COMMENT 'default timeslot',
  `email` varchar(45) DEFAULT NULL,
  `password_id` mediumint(6) unsigned NOT NULL,
  `user_name` varchar(45) NOT NULL COMMENT 'Name of the customer',
  `phone` bigint(20) unsigned NOT NULL,
  `otp` int(11) DEFAULT NULL,
  `address_id_1` mediumint(6) unsigned NOT NULL,
  `address_id_2` mediumint(6) unsigned DEFAULT NULL,
  `address_id_3` mediumint(6) unsigned DEFAULT NULL,
  `address_id_4` mediumint(6) unsigned DEFAULT NULL,
  `address_id_5` mediumint(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  KEY `password_idx` (`password_id`),
  KEY `address_idx` (`address_id_1`,`address_id_2`,`address_id_3`,`address_id_4`,`address_id_5`),
  KEY `ti_idx` (`def_timeslot`),
  KEY `u_addr2_idx` (`address_id_2`),
  KEY `u_addr3_idx` (`address_id_3`),
  KEY `u_addr4_idx` (`address_id_4`),
  KEY `u_addr5_idx` (`address_id_5`),
  CONSTRAINT `timeslot` FOREIGN KEY (`def_timeslot`) REFERENCES `timeslots` (`timeslot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `u_addr1` FOREIGN KEY (`address_id_1`) REFERENCES `address_details` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `u_addr2` FOREIGN KEY (`address_id_2`) REFERENCES `address_details` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `u_addr3` FOREIGN KEY (`address_id_3`) REFERENCES `address_details` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `u_addr4` FOREIGN KEY (`address_id_4`) REFERENCES `address_details` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `u_addr5` FOREIGN KEY (`address_id_5`) REFERENCES `address_details` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `u_password` FOREIGN KEY (`password_id`) REFERENCES `password_details` (`password_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Address';

LOCK TABLES `user_details` WRITE;
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;

INSERT INTO `user_details` (`user_id`, `def_timeslot`, `email`, `password_id`, `user_name`, `phone`, `otp`, `address_id_1`, `address_id_2`, `address_id_3`, `address_id_4`, `address_id_5`)
VALUES
	(1,2,'asdf',2,'Elan',7418209937,111,1,11,2,NULL,NULL),
	(2,1,'bsfb',5,'Hema',9444980759,NULL,2,12,13,NULL,NULL);

/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
