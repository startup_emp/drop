import express from 'express';
import client from './client-order';
import * as bodyParser from 'body-parser'
import {sign} from 'jsonwebtoken'
import linker from './linker'

const app = express()

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 
//app.use('/clientorder',client)
app.use('/',linker)

app.get('/',(req,res)=>{

  let signed = sign({userid:2},'shhhhh')
  res.send(signed)
})


app.post('/',(req,res)=>{
    console.log("her")
    res.send(req.body)
})

app.listen(2500, console.log("Server is up and running at port 2500 http://localhost:2500"))