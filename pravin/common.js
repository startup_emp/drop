import mysql from 'mysql'
import { sendMsg } from './fcm';


const db =   mysql.createPool({
    host:'localhost',
    database:'dropcan',
    user:'root',
    password:'C@rtoon123',
    connectionLimit: 10,
    supportBigNumbers: true
}, (e)=>{
    if(e)
        console.log(e)
    else 
        console.log("Connected to db")
});

export function queryfunction(q,value){
    return new Promise((resolve,reject)=>
    {   
        db.query(q,value,(err,result,feilds)=>{
            if(err)
                return reject(err)
            return resolve(result)
    })
})
}


export function sellerSingleOrderPush(order){
    orderdetails = {
        data:
        {
            "order-id": order.id,
            "order-checksum": order.checksum,
            "order-submission-date": order.date,
            "order-status": order.status,
        },
        token:order.fcmtoken
    }
    sendMsg()
}