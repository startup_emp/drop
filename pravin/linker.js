//Linker file
import express from 'express';
import client from './client-order';
import fcm from './fcm'


const app = express();

app.use('/client',client)
app.use('/fcm',fcm)

export default app


