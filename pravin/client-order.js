//////////////////////////////////
//Client Order processing script//
//////////////////////////////////
import {Router } from 'express'
import {queryfunction,logintoken} from './common'
import * as bodyParser from 'body-parser';
import express from 'express'
import { verify } from 'jsonwebtoken'
import {sendMsg } from './fcm'


//
const router = Router();
const app = express();

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 


//
router.post('/orderhistory',(req,res)=>{
    //Get exsisting order from the DB
      //  console.log("reached")
        queryfunction("select * from order_details where user_id = ?",req.body.userid).then(re => res.json(re)).catch(err=>res.json({error:err}));
    //Get exsisting from the Date

})

router.post('/sumbitorder',async (req,res)=>{
    //Decoding the JWT token
    let tokenDetails = verify(req.body.token,'shhhhh');
    //Payment check status
    let paymentStatus = req.body.paymentstatus == null ? false : true;
    //Check if the user exist in the database before pushing the order in to DB
    let user =  await queryfunction("select * from user_details where user_id=?",tokenDetails.userid).then(response => response[0]).catch(response => "error no user")
    //If the user detail retrived from the DB is 1 i.e if user account is their in DB then
    if(user && paymentStatus){
        //Order ID holder
        let orderId;
        //checksumcode holder
        let checksumcode;
          await queryfunction('insert into order_details(timeslot_id,address_id,quantity,user_id,phone) values(?,?,?,?,?)',
                                 [req.body.timeslot,req.body.addressid,req.body.quantity,user.user_id,user.phone]).then(response=> orederId = response.insertId)
        //Function call to generate the checksum
        //await checksum()
        await queryfunction('update')

        res.json({
            orderId:orderId,
            checksumcode:checksumcode,

        })
        
    }
    else if(user && !paymentStatus){
        await queryfunction('insert into order_details(timeslot_id,address_id,quantity,user_id,phone) values(?,?,?,?,?)',
            [req.body.timeslot,req.body.addressid,req.body.quantity,user.user_id,user.phone])
                .then(response=>{
                                res.json({orderstatus:"placed",
                                        })
                            }).catch(error=>console.log(error))

    }
    else{
        res.json({
            status:'error'
        })
    }

   
    res.send()
   // sendMsg(["testing",'test2','test3'])
        
})

router.post('/getauth',(req,res)=>{
    logintoken(req.body.phonenumber)
        .then(result=>{
            
        })
        .catch(err=>console.log("error from login token "+err))
 })

 router.get('/',(req,res)=>{
     res.send("Reached the ordering page!")
 })

export default router