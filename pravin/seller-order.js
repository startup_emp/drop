import {Router } from 'express'
import * as bodyparser from 'body-parser'

const router = Router();


router.post('/acceptorder',(req,res)=>{
    res.json({
        "status":'Accepted'
    });
});

export default router;