//------------------------------------------------------------------------------------------//
//                                                                                          //
//                  Firebase token validation and storing script                            // 
//                                                                                          //
//------------------------------------------------------------------------------------------//

//////////////////////////////////////////////
//Import module for the service requirement//
////////////////////////////////////////////
import {Router} from 'express'
import express from 'express'
import * as admin from 'firebase-admin';//Firebase module to connect to Admin console
import * as bodyParser from 'body-parser';//Body parser for accessing POST request
import mysql from 'mysql'; //mySQL DB module
import * as devicestatus from 'express-device'; // check device type to validate if the request was orginated from the phone and not from the PC/MAC

//test jwt
import { sign , verify } from 'jsonwebtoken'

//Importing query function
import {queryfunction} from './common'


////////////////////////////////////////////////////////
//  Constant value and constant data initialization
///////////////////////////////////////////////////////

//Private key json 
var serviceAccount = require('./seriveaccount.json');


//setting up express as app
const app = express()
const router = Router();

/////////////////////////////////
// Initiating APP.USE function 
////////////////////////////////

//setting up bodyparser
app.use(bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

//initiate GET request header collector
app.use(devicestatus.capture());


//////////////////////////////////////////////////
//
//Connecting to DB
//
//////////////////////////////////////////////////
// const db = mysql.createConnection({
//     host:'localhost',
//     database:'dropcan',
//     user:'root',
//     password:'C@rtoon123'
// }, (e)=>{
//     if(e)
//         console.log(e)
//     else 
//         console.log("Connected to db")
// });

/////////////////////////////////////////////////
//
//Initialization of Admin connection to firebase
//
/////////////////////////////////////////////////
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://drop-1dcfe.firebaseio.com'
  });


////////////////////////
//
//root page for checking
//
////////////////////////
router.get('/',(req,res)=>{
    res.send('Your landed on wrong page buddy!! Download the Drop app from playstore or AppStore to order bubble top water can')
})
////////////////////////////////////////////////////////
//
//FCM token inserting and updating functionality script
//
///////////////////////////////////////////////////////
router.post('/settoken',(req,res)=>{
        //if(req.device.type == "desktop"|| !req.device.type)
        // {
        //     res.send("Access denied")
        // }
    var tokenid = req.body.tokenid // get the tokenid from the request
    var uuid =  req.body.uuid  //Need to check if this is required or not
    var username = req.body.username // forgein key for table 
    var accessToken = !req.headers['x-access-token'] ? false : req.headers['x-access-token'];

    if(!accessToken)
    {
        res.send("No token");
    }
    else{

        queryfunction('select * from fcm_token where uuid= ? ',uuid).then(response=>{
            if(response.length ==0)
            {
                queryfunction('insert into fcm_token (tokenid,username,uuid) VALUES (?,?,?)',[tokenid,username,uuid])
                    .then(response=>{
                        res.json
                        ({
                            statuscode:200
                        })
                      })
                    .catch(error=>res.send({
                    statuscode:100
                     }))
                
            }
            else{
                res.send({
                    statuscode:100
                })
            }
        }).catch(error=>res.send({
            statuscode:100
        }))
       
    }
         
})

////////////////////////////////////
//Browser restriction
///////////////////////////////////
router.all("*",(req,res,err)=>{
    //If the user try to make an attempt to connect via PC then we need to block the access
    res.send("You are on wrone page, Download the app!")
})


///////////////////////////
// Message
// data with Key and Value
//////////////////////////
//Template
//////////////////////////
let  orderStatusDetails = {
    data:
    {
      orderid: '01',
      status: 'orderplaced',
      checksum:'',
      uuid:'',
    },
  };


///////////////////////////////////////////////////////
//  Function for triggering message from FCM to device
//////////////////////////////////////////////////////
export function sendMsg(message){
    message.forEach(element => {
      //  sending message to  each device
        admin.messaging().send(message)
            .then((response) => {
            // Response is a message ID string.
            console.log('Successfully sent message:', response);
            })
            .catch((error) => {
            console.log('Error sending message:', error);
            });
    });
}

//FCM connection 
export default router
