'use strict';

var express = require('express');
var app = express();
var mysql = require('mysql');
var bodyParser = require('body-parser');
//JWT Web Token
var jwt = require('jsonwebtoken');
var expressjwt = require('express-jwt');

//For Allowing Cross Origin Requests
var cors = require('cors');

var key = require("./constants").secretkey;
var host = require("./constants").host;
var db = require("./constants").db;
var username = require("./constants").username;
var password = require("./constants").password;

//Not using
var aes = require('crypto-js/aes');
var CryptoJS = require('crypto-js');
var crypto = require('crypto');
var sha = require('crypto-js/sha256');
var async = require('async');

app.use(cors()); //For Allowing Cross Origin Requests
//For getting body input in URL encoded format
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(expressjwt({ secret: key })); //Decrypt JWT to user object

//Create connection to mysql db
var connection = mysql.createConnection({
  host: host,
  database: db,
  user: username,
  password: password
});

//-----------First Page--------------------
/**Check if the Phone Number received from the client app is registered
 *Get all the Pincode and corrosponding Area available for the app*/
app.get('/login-check', function (req, res) {
  if (phone = req.query.phoneno) {
    console.log("Hi");
    /**Check if the Phone Number received from the client app is registered
     *Req=>/login-check?phoneno=7418209937
     *Res=>err || "error" || the phone number sent in the req
     */
    connection.query('select phone from drop.user where phone = ?', phone, function (err, rows, fields) {
      if (err) throw err;
      if (rows.length <= 0) {
        console.log("error");
        res.send("error");
      } else {
        console.log("rows");
        res.send(rows);
      }
    });
  } else {
    console.log("LOL 2");
    /**Get all the Pincode and corrosponding Area available for the app
     *Req=>/login-check
     *Res=>all pincode with its corrosponding area name
     */
    res.sendFile("./Res/pincode.json", {
      root: __dirname
    });
  }
});

//------------Second Page-------------------
/**Check if the password corrosponding to the phone number matches in the db 
 *Req=>/login?phoneno=7418209937&password=elan
 *Res=>err || "error" || the password corrosponding to the phone number || "Wrong Password"
 */
app.get('/login', function (req, res) {
  var phone = req.query.phoneno;
  var password = req.query.password;

  console.log(phone + " " + password);
  connection.query('select password from drop.user where phone = ?', phone, function (err, rows, fields) {
    if (err) throw err;
    if (rows.length <= 0) res.send("error");else if (password == rows[0].password) res.send(rows[0].password);else res.send("Wrong Passowrd");
  });
});

//---------------Third Page and Fourth Page--------------------
/**Get all the address related to a particular phone number
 *Req=>/getalladdress?phoneno=7418209937
 *Res=>err || "error" || all the address corrosponding to a phone number
 */

app.get('/address', function (req, res) {
  if (address = req.query.address) {
    var phone = req.query.phoneno;
    var address = req.query.address;
    console.log(phone + "\n" + address);
    connection.query('insert into drop.user_details(userid, address) select userid, ? from user where phone = ?;', [address, phone], function (err, rows, fields) {
      console.log("if");
      if (err) console.log(err);
      /*if (rows.length <= 0)
      res.send("error");
      else
      rows.forEach(function(row){
      address.push(row);*/
      connection.query('select last_insert_id() as last from `drop`.user_details;', function (err, rows, fields) {
        if (err) console.log(err);
        if (rows.length <= 0) res.send("length 0");else res.send(rows[0].last.toString());
      });
    });
  } else {
    console.log("else");
    var phone = req.query.phoneno;
    var address = [];
    connection.query('select address from drop.user_details ud, user u where u.phone = ? and ud.userid = u.userid;', phone, function (err, rows, fields) {
      if (err) throw err;
      if (rows.length <= 0) res.send("error");else rows.forEach(function (row) {
        address.push(row);
      });
      res.send(address);
    });
  }
});

//---------------Time Slot and Quantity Page--------------
//---------------Place Order Page---------------------
app.post('/placeorder', function (req, res) {
  var phone = req.body.phoneno;
  var timeslot = req.body.timeSlot;
  var address = req.body.address;
  var resultid;

  console.log(phone + " " + timeslot + " " + address);
  connection.query('insert into `drop`.order(phone, timeslot, address) values(?, ?, ?);', [phone, timeslot, address], function (err, result) {
    if (err) console.log(err);
    /*connection.query('select orderid from `drop`.order where phone = ? and timeslot = ? ;', [phone, timeslot], function (err) {
      if (err) console.log(err);
      res.send("address");
    });*/
    //get the Primary key/Auto Generated column during the insert command
    resultid = result.insertId;

    /*connection.query('SELECT LAST_INSERT_ID();', [phone, timeslot], function (err, rows, fields) {
      if (err) console.log(err);
      res.send(rows[0].LAST_INSERT_ID());
    });*/
    connection.query('update `drop`.order set sellerid = ? where orderid = ?;', ["ela123", resultid], function (err) {
      if (err) console.log(err);else console.log(resultid);
    });
    res.send(resultid.toString());
  });
});

//------------Order confirmation page---------------

//------------Order confirmation Page Ends---------------

//------------Registration Page Starts---------------

app.post('/register', function (req, res) {
  var phone = req.body.phoneno;
  var password = req.body.password;

  console.log(req.body);
});
//------------Registration Page Ends---------------
var phoneandpassword = function phoneandpassword(phoneno) {
  var phone;
  connection.query('select phone,password from drop.user where phone = ?', phoneno, function (err, rows, fields) {
    if (err) console.log(err);
    phone = rows[0].password;
    //console.log(rows[0].password + " " +phone);
    //console.log(phone + 1);
    /*connection.query('insert into drop.user values (?,?)', [phone + 1, password], function (err) {
      if (err) throw err
    });*/
  });
  return phone;
};

/*app.get('/:blocks', function (req, res) {
  var phone, phoneno = req.params.blocks;

  phoneandpassword(phoneno);
  async.series([
    phoneandpassword(phoneno)
  ]).call().end(function (err, result) {
    phone = result;
    console.log(phone);
  });
  console.log(phone);
});*/

app.get('/a', function (req, res) {
  res.send(encrypt("elan"));
});
/*var encrypt = function (plain) {
  //Encryption and Decryption start
  console.log(pass + "\n\nEncrypt:\n");
  var enc = aes.encrypt(pass, "elan");
  console.log(enc + "\n\nDecrypt:\n");
  var dec = aes.decrypt(enc, "elan").toString(CryptoJS.enc.Utf8);
  console.log(dec + "\n\nPass:");
  //Encryption and Decryption ends

  //One Way Hashing starts
  console.log(pass + "\n\nEncrypt:\n");
  var enc = sha(pass).toString();;
  console.log(enc + "\n");
  var dec = sha(pass).toString();;
  console.log(dec);
  //One Way Hashing ends
};*/

var genRandomString = function genRandomString(length) {
  return crypto.randomBytes(Math.ceil(length / 2)).toString('hex') /** convert to hexadecimal format */
  .slice(0, length); /** return required number of characters */
};
app.listen(8081, function () {
  console.log("Listening");
});

var encrypt = function encrypt(password) {
  var salt = genRandomString(4);
  var hash = crypto.createHmac('sha1', "85ef");
  hash.update(password);
  var value = hash.digest('hex');
  return {
    salt: salt,
    passwordHash: value
  };
};