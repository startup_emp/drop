'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _mysql = require('mysql');

var _bodyParser = require('body-parser');

var _fs = require('fs');

var _logger = require('./route/common/logger');

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _constants = require('./constants');

var _firstPage = require('./route/client/first-page');

var _firstPage2 = _interopRequireDefault(_firstPage);

var _loginPage = require('./route/client/login-page');

var _loginPage2 = _interopRequireDefault(_loginPage);

var _newuser = require('./route/client/newuser');

var _newuser2 = _interopRequireDefault(_newuser);

var _aes = require('crypto-js/aes');

var _aes2 = _interopRequireDefault(_aes);

var _cryptoJs = require('crypto-js');

var _cryptoJs2 = _interopRequireDefault(_cryptoJs);

var _crypto = require('crypto');

var _sha = require('crypto-js/sha256');

var _sha2 = _interopRequireDefault(_sha);

var _async = require('async');

var _async2 = _interopRequireDefault(_async);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ------------------------- Require Ends -------------------------------


// Routes


// For Allowing Cross Origin Requests
var app = (0, _express2.default)();
// ------------------------- Middleware Starts --------------------------


// Not using


// var key = require('./constants').secretkey

// const routes = require('routes');

// logging

app.use((0, _cors2.default)()); // For Allowing Cross Origin Requests
// For getting body input in URL encoded format
app.use((0, _bodyParser.urlencoded)({
  extended: true
}));
app.use((0, _bodyParser.json)());

// ------------------------- Middleware Ends --------------------------

// ------------------------- Initialization Code Starts -----------------------
var phone = null;

// Create connection to mysql db
var connection = (0, _mysql.createConnection)({
  host: _constants.host,
  database: _constants.db,
  user: _constants.username,
  password: _constants.password,
  multipleStatements: _constants.multipleStatements
});

// Run this function everyday to update pincode.json from db
function updatePincode() {
  console.log('This pops up every 5 seconds and is annoying!');
  connection.query('select * from drop.pincode_details', function (err, rows, fields) {
    if (err) {
      _logger.logger.error(err);
      throw err;
    }
    (0, _fs.writeFileSync)('./Res/pincode.json', JSON.stringify(rows), 'utf-8');
  });
}
// ------------------------- Initialization Code Ends -----------------------

// routes
//  Connect all our routes to our application
app.use('/', _firstPage2.default);
app.use('/', _loginPage2.default);
app.use('/', _newuser2.default);
/* require('./code/client/first-page')(app, connection);
require('./code/client/newuser')(app, connection);
require('./code/client/login-page')(app, connection); */

/* app.use(
  expressjwt({
    secret: key
  }),
  function (req, res, next) {

    logger.info('Hello world');
    logger.warn('Warning message');
    logger.debug('Debugging info');
    console.log("R" + req.user.userid);
    phone = req.user.userid;
    next();
  }
); */

/* app.use(
  jwt.verify(token, key, function (err, decoded, next) {
    console.log(decoded.userid);
    next();
  })
); */ // Middleware for Decrypting JWT to user object

// ---------------Third Page and Fourth Page--------------------
/** Get all the address related to a particular phone number
 *Req=>/address?phoneno=7418209937
 *Res=>err || "error" || all the address corrosponding to a phone number
 */

app.get('/address', function (req, res) {
  var phone = req.query.phoneno;
  var address = [];
  var flag = 0;
  var result = {};

  connection.query('select address_id_1, address_id_2, address_id_3, address_id_4, address_id_5 from drop.user_details where phone = ?', phone, function (err, rows, fields) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
      };
      _logger.logger.error(err);
      res.send(result);
      throw err;
    } else if (rows.length <= 0) {
      result = {
        'output': 'error'
      };
      res.send(result);
    } else {
      var _loop = function _loop(i) {
        var address_str = 'address_id_' + i.toString();
        connection.query('select * from drop.address_details where address_id = ?', rows[0][address_str], function (err, row_s, fields) {
          if (err) {
            result = {
              'output': 'error',
              'error': 'unknown'
            };
            _logger.logger.error(err);
            res.send(result);
            throw err;
          }
          var pincode_id = row_s.length == 0 ? 0 : row_s[0].pincode_id;
          connection.query('select pincode from drop.pincode_details where pincode_id = ?', pincode_id, function (err, row_t, fields) {
            if (err) {
              result = {
                'output': 'error',
                'error': 'unknown'
              };
              _logger.logger.error(err);
              res.send(result);
              throw err;
            } else if (row_t.length != 0) {
              row_s[0].pincode = row_t[0].pincode;
              address.push(row_s[0]);
            }
            if (i === 5) {
              res.send(address);
            }
          });
        });
      };

      for (var i = 1; i <= 5; i++) {
        _loop(i);
      }
    }
  });
});

/** Add a new address to a particular phone number
 *Req=>/address
 *   =>Body Form-URL-Encoded => phoneno:7418209937, address:asf-8848-asd-asd-mana-sdfgsd-1-asdf
 *Res=>err || "error" || all the address corrosponding to a phone number
 */
app.post('/address', function (req, res) {
  var phone = req.body.phoneno;
  var address = req.body.address;
  var address_str = null;
  var address_id = null;
  var result = {};
  console.log(phone + '\n' + address);
  var arr = address.split('**');
  query_str = 'CALL `USP_AddNewAddress`(?, ?, ?, ?, ?, ?, ?, ?,?,@output_param);SELECT @output_param';
  connection.query(query_str, [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], phone], function (err, rows, fields) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
        // logger.error(err);
      };res.send(result);
      throw err;
    }
    // console.log(rows[1][0]['@output_param'].toString());
    else {
        result = {
          'output': 'Success',
          'error': 'none',
          'InsertedRowId': rows[1][0]['@output_param'].toString()
        };
        console.log(rows[1][0]['@output_param'].toString());
        res.send(result);
      }
  });
  // connection.query('insert into drop.address_details(name, phone, door_no, street, area, town_city, pincode_id, landmark) values(?, ?, ?, ?, ?, ?, ?, ?)', [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7]], function (err, rows, fields) {
  //   if (err) {
  //     result = {
  //       "output": "error",
  //       "error": "unknown"
  //     };
  //     logger.error(err);
  //     res.send(result);
  //     throw err;
  //   }
  //   address_id = rows.insertId;
  //   console.log(address_id);
  //   if (rows.length <= 0) {
  //     result = {
  //       "output": "No rows Inserted",
  //       "error": "none",
  //       "InsertedRows": rows.insertId.toString()
  //     };
  //     res.send(result);
  //   } else {
  //     connection.query('select address_id_1, address_id_2, address_id_3, address_id_4, address_id_5 from drop.user_details where phone = ?', phone, function (err, rows, fields) {
  //       if (err) {
  //         result = {
  //           "output": "error",
  //           "error": "unknown"
  //         };
  //         logger.error(err);
  //         res.send(result);
  //         throw err;
  //       } else
  //       if (rows[0].address_id_1 == null)
  //         address_str = "address_id_1";
  //       else if (rows[0].address_id_2 == null)
  //         address_str = "address_id_2";
  //       else if (rows[0].address_id_3 == null)
  //         address_str = "address_id_3";
  //       else if (rows[0].address_id_4 == null)
  //         address_str = "address_id_4";
  //       else if (rows[0].address_id_5 == null)
  //         address_str = "address_id_5";

  //       if (address_str != null) {
  //         connection.query('update drop.user_details set ?? = ? where phone = ?', [address_str, address_id, phone], function (err, result) {
  //           if (err) {
  //             result = {
  //               "output": "error",
  //               "error": "unknown"
  //             };
  //             logger.error(err);
  //             res.send(result);
  //             throw err;
  //           }
  //           console.log("End" + result.affectedRows);
  //           result = {
  //             "output": "Success",
  //             "error": "none",
  //             "InsertedRowId": address_id.toString()
  //           };
  //           res.send(result);
  //         });
  //       }
  //     });
  //   }

  // });
});

/** Update the address of a particular phone number
 *Req=>/address/10
 *   =>/Body Form-URL-Encoded => phoneno:7418209937, address:asf-888-asd-asd-mana-sdfgsd-1-asdf
 *Res=>err || "error" || all the address corrosponding to a phone number
 */
app.patch('/address/:address_id', function (req, res) {
  var address_id = req.params.address_id;
  // var phone = req.body.phoneno;
  var address = req.body.address;
  var arr = address.split('**');
  connection.query('update drop.address_details  set name = ?, phone = ?, door_no = ?, street = ?, area = ?, town_city = ?, pincode_id = ?, landmark = ? where address_id = ?', [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], address_id], function (err, result) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
      };
      _logger.logger.error(err);
      res.send(result);
      throw err;
    } else if (result.affectedRows <= 0) {
      result = {
        'output': 'No rows updated',
        'error': 'none',
        'affectedRows': result.affectedRows.toString()
      };
      res.send(result);
    } else {
      result = {
        'output': 'Success',
        'error': 'none',
        'affectedRows': result.affectedRows.toString()
      };
      res.send(result);
    }
  });
});

/** Delete the address of a particular phone number
 *Req=>/address/10
 *   =>/Body Form-URL-Encoded => phoneno:7418209937, address:asf-888-asd-asd-mana-sdfgsd-1-asdf
 *Res=>err || "error" || all the address corrosponding to a phone number
 */
app.delete('/address/:address_id', function (req, res) {
  var address_id = req.params.address_id;

  var address_str = null;
  console.log(phone + address_id);
  connection.query('select address_id_1, address_id_2, address_id_3, address_id_4, address_id_5 from drop.user_details where phone = ?', phone, function (err, rows, fields) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
      };
      _logger.logger.error(err);
      res.send(result);
      throw err;
    }
    switch (address_id.toString()) {
      case rows[0].address_id_1 != null ? rows[0].address_id_1.toString() : null:
        address_str = 'address_id_1';
        break;
      case rows[0].address_id_2 != null ? rows[0].address_id_2.toString() : null:
        address_str = 'address_id_2';
        break;
      case rows[0].address_id_3 != null ? rows[0].address_id_3.toString() : null:
        address_str = 'address_id_3';
        break;
      case rows[0].address_id_4 != null ? rows[0].address_id_4.toString() : null:
        address_str = 'address_id_4';
        break;
      case rows[0].address_id_5 != null ? rows[0].address_id_5.toString() : null:
        address_str = 'address_id_5';
        break;
    }
    console.log(address_str);
    if (address_str != null) {
      connection.query('update drop.user_details set ?? = NULL where ?? = ?', [address_str, address_str, address_id], function (err, result) {
        if (err) {
          result = {
            'output': 'error',
            'error': 'unknown'
          };
          _logger.logger.error(err);
          res.send(result);
          throw err;
        } else if (result.affectedRows != 0) {
          connection.query('delete from drop.address_details where address_id = ?', address_id, function (err, result) {
            if (err) throw err;
            result = {
              'output': 'Success',
              'error': 'none',
              'affectedRows': result.affectedRows.toString()
            };
            res.send(result);
          });
        }
      });
    }
  });
});

// ---------------Time Slot and Quantity Page--------------
// ---------------Place Order Page---------------------
app.post('/placeorder', function (req, res) {
  console.log(req.body.phoneno);
  // var phone = req.body.phoneno != null ? req.body.phoneno: null;
  var timeSlot_id = req.body.timeSlot_id;
  var address_id = req.body.address_id;
  var quantity = req.body.quantity;
  var phoneno = req.body.phoneno;
  var resultid;

  console.log(phone + ' ' + timeSlot_id + ' ' + address_id + ' ' + quantity);
  connection.query('insert into drop.order_details(phone, timeslot_id, address_id, quantity) values(?, ?, ?, ?);', [phoneno, timeSlot_id, address_id, quantity], function (err, result) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
      };
      _logger.logger.error(err);
      res.send(result);
      throw err;
    }
    // get the Primary key/Auto Generated column during the insert command
    resultid = result.insertId;
    result = {
      'output': 'Success',
      'error': 'none',
      'orderid': result.insertId
    };
    res.send(result);
    /* connection.query('update `drop`.order set sellerid = ? where orderid = ?;', ["ela123", resultid], function (err) {
      if (err) console.log(err);
      else
        console.log(resultid);
    }); */
  });
});

app.post('/orderhistory', function (req, res) {
  var phone = req.body.phoneno;
  var sql = 'CALL USP_Order_History(?)';

  connection.query(sql, phone, function (error, results, fields) {
    if (error) {
      return console.error(error.message);
    }
    console.log(results[0]);
  });
});

// ------------Order confirmation page---------------

// ------------Order confirmation Page Ends---------------

// ------------Catch wrong Routes starts---------------
app.all('*', function (req, res) {
  console.log('wrong route');
});
// ------------Catch wrong Routes Ends---------------

var phoneandpassword = function phoneandpassword(phoneno) {
  var phone;
  connection.query('select phone,password from drop.user where phone = ?', phoneno, function (err, rows, fields) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
      };
      _logger.logger.error(err);
      res.send(result);
      throw err;
    }
    phone = rows[0].password;
    // console.log(rows[0].password + " " +phone);
    // console.log(phone + 1);
    /* connection.query('insert into drop.user values (?,?)', [phone + 1, password], function (err) {
      if (err) throw err
    }); */
  });
  return phone;
};

/* app.get('/:blocks', function (req, res) {
  var phone, phoneno = req.params.blocks;

  phoneandpassword(phoneno);
  async.series([
    phoneandpassword(phoneno)
  ]).call().end(function (err, result) {
    phone = result;
    console.log(phone);
  });
  console.log(phone);
}); */

/* var encrypt = function (plain) {
  //Encryption and Decryption start
  console.log(pass + "\n\nEncrypt:\n");
  var enc = aes.encrypt(pass, "elan");
  console.log(enc + "\n\nDecrypt:\n");
  var dec = aes.decrypt(enc, "elan").toString(CryptoJS.enc.Utf8);
  console.log(dec + "\n\nPass:");
  //Encryption and Decryption ends

  //One Way Hashing starts
  console.log(pass + "\n\nEncrypt:\n");
  var enc = sha(pass).toString();;
  console.log(enc + "\n");
  var dec = sha(pass).toString();;
  console.log(dec);
  //One Way Hashing ends
}; */

var genRandomString = function genRandomString(length) {
  return (0, _crypto.randomBytes)(Math.ceil(length / 2)).toString('hex') /** convert to hexadecimal format */
  .slice(0, length); /** return required number of characters */
};

app.listen(process.env.PORT || 8099, function () {
  // logging();

  // setInterval(updatePincode, 5000);
  // geoCode();
  console.log('Listening now in' + process.env.PORT);
});

var geoCode = function geoCode() {
  var googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyAu4yAiXkcs28yrPMl_iIkW61FfAQkU4DE'
  });

  // Geocode an address.
  googleMapsClient.geocode({
    address: 'No.2, 4th Avenue, Pallava Garden, Zamin Pallavaram, Chennai - 600117'
  }, function (err, response) {
    if (!err) {
      console.log(response.json.results);
    }
  });
};

var encrypt = function encrypt(password) {
  var salt = genRandomString(4);
  var hash = (0, _crypto.createHmac)('sha1', '85ef');
  hash.update(password);
  var value = hash.digest('hex');
  return {
    salt: salt,
    passwordHash: value
  };
};