'use strict'

Object.defineProperty(exports, '__esModule', {
  value: true
})
exports.loginCheck = loginCheck
exports.login = login
exports.getPassword = getPassword
exports.addAddress = addAddress
exports.updateAddress = updateAddress
exports.placeOrder = placeOrder
exports.insertPassword = insertPassword
exports.addNewUser = addNewUser

var _mysql = require('mysql')

var _config = require('./config')

var host = _config.dbConfig.dbEndpoint

// Db configuration

var user = _config.dbConfig.userName
var password = _config.dbConfig.password
var dbName = _config.dbConfig.dbName

var pool = (0, _mysql.createPool)({
  host: host,
  user: user,
  password: password,
  database: dbName,
  connectionLimit: 10,
  supportBigNumbers: true
})

// first page
function loginCheck (phone, callback) {
  var sql = 'select user_id from user_details where phone = ?'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [phone], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(false, results)
    })
  })
}

// login page
function login (phone, callback) {
  var sql = 'select password_id from user_details where phone = ?'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [phone], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(false, results)
    })
  })
}

function getPassword (passwordId, callback) {
  var sql = 'select password from password_details where password_id = ?'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [passwordId], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(false, results)
    })
  })
}

// functions for new user
function addAddress (arr, callback) {
  var sql = 'insert into address_details(name, phone, door_no, street, area, town_city, pincode_id, landmark) values(?, ?, ?, ?, ?, ?, ?, ?)'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7]], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(false, results)
    })
  })
}

function updateAddress (arr, addressId, callback) {
  var sql = 'update address_details  set name = ?, phone = ?, door_no = ?, street = ?, area = ?, town_city = ?, pincode_id = ?, landmark = ? where address_id = ?'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], addressId], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(false, results)
    })
  })
}

function placeOrder (phone, timeSlotId, addressId, quantity, callback) {
  var sql = 'insert into order_details(phone, timeslot_id, address_id, quantity) values(?, ?, ?, ?)'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [phone, timeSlotId, addressId, quantity], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(false, results)
    })
  })
}

function insertPassword (hash, callback) {
  var sql = 'insert into drop.password_details(password) values(?)'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, hash, function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(false, results)
    })
  })
}

function addNewUser (insertId, username, phone, addressId, callback) {
  var sql = 'insert into drop.user_details( def_timeslot, password_id,user_name,phone,address_id_1) values(?,?,?,?,?)'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [1, insertId, username, phone, addressId], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(false, results)
    })
  })
}
