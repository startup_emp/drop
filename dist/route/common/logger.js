'use strict';

var winston = require('winston');

var _require = require('winston'),
    createLogger = _require.createLogger,
    format = _require.format,
    transports = _require.transports; //Hema Code change


var combine = format.combine,
    timestamp = format.timestamp,
    label = format.label,
    printf = format.printf; //Hema Code change

function logging() {

    var myFormat = printf(function (info) {
        return info.timestamp + ' [' + info.label + '] ' + info.level + ': ' + info.message.json();
    });

    logger = createLogger({
        format: combine(label({ label: 'right meow!' }), timestamp(), format.json(), myFormat),
        transports: [new winston.transports.File({
            filename: 'combined.log',
            timestamp: false
        })]
    });
    var tsFormat = function tsFormat() {
        return new Date().toLocaleTimeString();
    };

    //
    // If we're not in production then log to the `console` with the format:
    // `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
    // 
    if (process.env.NODE_ENV !== 'production') {
        logger.add(new winston.transports.Console({
            format: winston.format.simple()
        }));
    }
}

module.exports.logger = winston.createLogger({ //Hema Code change
    level: 'info',
    format: combine(timestamp(), format.json()),
    transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log` 
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.File({
        filename: 'error.log',
        level: 'error'
    }), new winston.transports.File({
        filename: 'combined.log',
        timestamp: false
    }), new winston.transports.Console()]
});