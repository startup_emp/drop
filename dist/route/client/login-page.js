'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _bcryptNodejs = require('bcrypt-nodejs');

var _jsonwebtoken = require('jsonwebtoken');

var _constants = require('../../constants');

var _query = require('../common/query');

var _express = require('express');

// Hema code ends

var router = (0, _express.Router)();

/** Check if the password corrosponding to the phone number matches in the db
 *Req=>/login
 *=> Body Form-URL-Encoded => phoneno:7418209937, password:elan
 *Res=>err || "error" || the password corrosponding to the phone number || "Wrong Password"
 */
// Hema code starts
router.post('/login', function (req, res) {
  var phone = req.body.phoneno;
  var password = req.body.password;
  var result = {};
  (0, _query.login)(phone, function (err, rows) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
        // logger.error(err);
      };res.send(result);
      throw err;
    } else if (rows.length <= 0) {
      result = {
        'output': 'error',
        'error': 'user_not_found'
      };
      res.send(result);
    } else {
      var passwordId = rows[0].password_id;
      (0, _query.getPassword)(passwordId, function (err, rows) {
        if (err) {
          result = {
            'output': 'error',
            'error': 'unknown'
            // logger.error(err);
          };res.send(result);
          throw err;
        }
        (0, _bcryptNodejs.compare)(password, rows[0].password, function (err, out) {
          if (err) {
            result = {
              'output': 'error',
              'error': 'unknown'
              // logger.error(err);
            };res.send(result);
            throw err;
          } else if (out) {
            var token = (0, _jsonwebtoken.sign)({
              userid: phone
            }, _constants.secretkey);
            result = {
              'output': 'true',
              'bearer': token
            };
            res.send(result);
          } else {
            result = {
              'output': 'error',
              'error': 'wrong_password'
            };
            res.send(result);
          }
        });
      });
    }
  });
});
exports.default = router;