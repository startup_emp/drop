-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: drop
-- ------------------------------------------------------
-- Server version	5.6.25-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address_details`
--

DROP TABLE IF EXISTS `address_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_details` (
  `address_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `door_no` varchar(45) NOT NULL,
  `street` varchar(45) NOT NULL,
  `area` varchar(45) NOT NULL,
  `town_city` varchar(45) NOT NULL,
  `pincode_id` mediumint(6) unsigned NOT NULL,
  `landmark` varchar(45) NOT NULL,
  `state` char(15) NOT NULL DEFAULT 'tamilnadu',
  PRIMARY KEY (`address_id`),
  KEY `pincode_idx` (`pincode_id`),
  KEY `area_idx` (`area`),
  CONSTRAINT `a_pincode` FOREIGN KEY (`pincode_id`) REFERENCES `pincode_details` (`pincode_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `area` FOREIGN KEY (`area`) REFERENCES `pincode_details` (`area`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_details`
--

LOCK TABLES `address_details` WRITE;
/*!40000 ALTER TABLE `address_details` DISABLE KEYS */;
INSERT INTO `address_details` VALUES (1,'asf',8848,'asd','asd','mana','sdfgsd',1,'asdf','tamilnadu'),(2,'asdfsdf',6262,'arfr','asdf','palla','ergerg',2,'egrdg','tamilnadu'),(5,'rh',7656,'dgn','dfb','anna','dfbdb',3,'bdfgn','tamilnadu'),(6,'asf',8848,'asd','asd','mana','sdfgsd',1,'asdf','tamilnadu'),(7,'asf',8848,'asd','asd','mana','sdfgsd',1,'asdf','tamilnadu'),(8,'asf',8848,'asd','asd','mana','sdfgsd',1,'asdf','tamilnadu'),(9,'asf',848,'asd','asd','mana','sgsd',1,'asd','tamilnadu'),(11,'asf',8848,'asd','asd','mana','sdfgsd',1,'asdf','tamilnadu'),(12,'asf',8848,'asd','asd','mana','sdfgsd',1,'asdf','tamilnadu'),(13,'asf',8848,'asd','asd','mana','sdfgsd',1,'asdf','tamilnadu');
/*!40000 ALTER TABLE `address_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_details` (
  `order_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `phone` bigint(10) unsigned NOT NULL,
  `timeslot_id` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `seller_id` mediumint(6) unsigned DEFAULT NULL,
  `address_id` mediumint(6) unsigned NOT NULL,
  `quantity` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `user_id` mediumint(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `address_idx` (`address_id`),
  KEY `o_tiemslot_idx` (`timeslot_id`),
  KEY `o_sellert_id_idx` (`seller_id`),
  KEY `o_user_id_idx` (`user_id`),
  CONSTRAINT `o_address` FOREIGN KEY (`address_id`) REFERENCES `address_details` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `o_sellert_id` FOREIGN KEY (`seller_id`) REFERENCES `seller_details` (`s_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `o_timeslot` FOREIGN KEY (`timeslot_id`) REFERENCES `timeslots` (`timeslot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `o_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_details` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
INSERT INTO `order_details` VALUES (2,7418209937,2,NULL,5,3,NULL);
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_details`
--

DROP TABLE IF EXISTS `password_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_details` (
  `password_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `password` varchar(60) NOT NULL,
  `pepper` varchar(4) NOT NULL,
  PRIMARY KEY (`password_id`),
  UNIQUE KEY `pepper_UNIQUE` (`pepper`),
  UNIQUE KEY `password_UNIQUE` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Password for both user and seller';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_details`
--

LOCK TABLES `password_details` WRITE;
/*!40000 ALTER TABLE `password_details` DISABLE KEYS */;
INSERT INTO `password_details` VALUES (2,'$2a$10$xh2j7TRKQ0C4r6vvvhKaKeybXiWZso4NzgoQFzFDJZV..CXIOrl46','asfd'),(5,'$2a$10$L7S9Knz/cHcxqUEnBsacie6FQnntw.HaXKpA4yRw2vYuZCgzon3oW','gerg');
/*!40000 ALTER TABLE `password_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_details`
--

DROP TABLE IF EXISTS `payment_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_details` (
  `payment_id` varchar(12) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_details`
--

LOCK TABLES `payment_details` WRITE;
/*!40000 ALTER TABLE `payment_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pincode_details`
--

DROP TABLE IF EXISTS `pincode_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pincode_details` (
  `pincode_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `pincode` mediumint(9) unsigned NOT NULL,
  `area` varchar(45) NOT NULL,
  PRIMARY KEY (`pincode_id`),
  UNIQUE KEY `area_UNIQUE` (`area`),
  UNIQUE KEY `area_pin_unique` (`pincode`,`area`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pincode_details`
--

LOCK TABLES `pincode_details` WRITE;
/*!40000 ALTER TABLE `pincode_details` DISABLE KEYS */;
INSERT INTO `pincode_details` VALUES (7,600001,'anna'),(1,600101,'chen'),(3,600101,'kanche'),(4,600101,'palla'),(2,600102,'madur'),(6,600111,'tnj'),(5,600117,'mana');
/*!40000 ALTER TABLE `pincode_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seller_details`
--

DROP TABLE IF EXISTS `seller_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seller_details` (
  `s_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `s_name` varchar(30) NOT NULL,
  `s_address_id` mediumint(6) unsigned NOT NULL,
  `phone` bigint(20) unsigned NOT NULL,
  `timeslot_id` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `quantity` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `password_id` mediumint(6) unsigned NOT NULL,
  `location_lat` point DEFAULT NULL,
  `location_long` point DEFAULT NULL,
  `s_status` set('registered','available','not_available') NOT NULL DEFAULT 'registered',
  `out_f_seller` int(11) unsigned DEFAULT NULL COMMENT 'Outstanding amount from seller',
  `out_t_seller` int(11) unsigned DEFAULT NULL COMMENT 'Outstanding amount to seller',
  `today_qty` tinyint(3) unsigned DEFAULT NULL,
  `today_amt_t_seller` int(11) unsigned DEFAULT NULL,
  `today_amt_f_seller` int(10) unsigned DEFAULT NULL,
  `area` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`s_id`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  KEY `address_idx` (`s_address_id`),
  KEY `timeslot_idx` (`timeslot_id`),
  KEY `s_password_idx` (`password_id`),
  CONSTRAINT `s_address` FOREIGN KEY (`s_address_id`) REFERENCES `address_details` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `s_password` FOREIGN KEY (`password_id`) REFERENCES `password_details` (`password_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `s_timeslot` FOREIGN KEY (`timeslot_id`) REFERENCES `timeslots` (`timeslot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller_details`
--

LOCK TABLES `seller_details` WRITE;
/*!40000 ALTER TABLE `seller_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `seller_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timeslots`
--

DROP TABLE IF EXISTS `timeslots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeslots` (
  `timeslot_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `timeslot` set('8-12','2-6','6-10') NOT NULL,
  PRIMARY KEY (`timeslot_id`),
  UNIQUE KEY `timeslot_UNIQUE` (`timeslot`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='For containing timeslots as the foreign key.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timeslots`
--

LOCK TABLES `timeslots` WRITE;
/*!40000 ALTER TABLE `timeslots` DISABLE KEYS */;
INSERT INTO `timeslots` VALUES (1,'8-12'),(2,'2-6'),(3,'6-10');
/*!40000 ALTER TABLE `timeslots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_details` (
  `user_id` mediumint(6) unsigned NOT NULL AUTO_INCREMENT,
  `def_timeslot` tinyint(3) unsigned DEFAULT NULL COMMENT 'default timeslot',
  `email` varchar(45) DEFAULT NULL,
  `password_id` mediumint(6) unsigned NOT NULL,
  `user_name` varchar(45) NOT NULL COMMENT 'Name of the customer',
  `phone` bigint(20) unsigned NOT NULL,
  `otp` int(11) DEFAULT NULL,
  `address_id_1` mediumint(6) unsigned NOT NULL,
  `address_id_2` mediumint(6) unsigned DEFAULT NULL,
  `address_id_3` mediumint(6) unsigned DEFAULT NULL,
  `address_id_4` mediumint(6) unsigned DEFAULT NULL,
  `address_id_5` mediumint(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `phone_UNIQUE` (`phone`),
  KEY `password_idx` (`password_id`),
  KEY `address_idx` (`address_id_1`,`address_id_2`,`address_id_3`,`address_id_4`,`address_id_5`),
  KEY `ti_idx` (`def_timeslot`),
  KEY `u_addr2_idx` (`address_id_2`),
  KEY `u_addr3_idx` (`address_id_3`),
  KEY `u_addr4_idx` (`address_id_4`),
  KEY `u_addr5_idx` (`address_id_5`),
  CONSTRAINT `timeslot` FOREIGN KEY (`def_timeslot`) REFERENCES `timeslots` (`timeslot_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `u_addr1` FOREIGN KEY (`address_id_1`) REFERENCES `address_details` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `u_addr2` FOREIGN KEY (`address_id_2`) REFERENCES `address_details` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `u_addr3` FOREIGN KEY (`address_id_3`) REFERENCES `address_details` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `u_addr4` FOREIGN KEY (`address_id_4`) REFERENCES `address_details` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `u_addr5` FOREIGN KEY (`address_id_5`) REFERENCES `address_details` (`address_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `u_password` FOREIGN KEY (`password_id`) REFERENCES `password_details` (`password_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Address';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_details`
--

LOCK TABLES `user_details` WRITE;
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
INSERT INTO `user_details` VALUES (1,2,'asdf',2,'Elan',7418209937,111,1,11,2,NULL,NULL),(2,1,'bsfb',5,'Hema',9444980759,NULL,2,12,13,NULL,NULL);
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'drop'
--

--
-- Dumping routines for database 'drop'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-06 22:51:50
