import { addAddress, placeOrder, insertPassword, addNewUser } from '../common/query'
import { Router } from 'express'
import { genSalt, hash as _hash } from 'bcrypt-nodejs'
var router = Router()

/** Add a new address to a particular phone number
     *Req=>/address
     *   =>Body Form-URL-Encoded => phoneno:7418209937, address:asf-8848-asd-asd-mana-sdfgsd-1-asdf
     *Res=>err || "error" || all the address corrosponding to a phone number
     */
router.post('/address-new', function (req, res) {
  var phone = req.body.phoneno
  var address = req.body.address
  var result = {}
  console.log(phone + '\n' + address)
  var arr = address.split('**')
  addAddress(arr, function (err, rows) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
      }
      // logger.error(err);
      res.send(result)
      throw err
    }
    var addressId = rows.insertId
    console.log(addressId)
    result = {
      'output': 'Success',
      'error': 'none',
      'InsertedRowId': addressId.toString()
    }
    res.send(result)
  })
})

/** Update the address of a particular phone number
     *Req=>/address/10
     *   =>/Body Form-URL-Encoded => phoneno:7418209937, address:asf-888-asd-asd-mana-sdfgsd-1-asdf
     *Res=>err || "error" || all the address corrosponding to a phone number
     */
router.patch('/address-new/:address_id', function (req, res) {
  var addressId = req.params.address_id
  var address = req.body.address
  var arr = address.split('**')
  var result = {}
  addAddress(arr, addressId, function (err, resp) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
      }
      // logger.error(err);
      res.send(result)
      throw err
    } else
    if (res.affectedRows <= 0) {
      result = {
        'output': 'No rows updated',
        'error': 'none',
        'affectedRows': resp.affectedRows.toString()
      }
      res.send(result)
    } else {
      result = {
        'output': 'Success',
        'error': 'none',
        'affectedRows': resp.affectedRows.toString()
      }
      res.send(result)
    }
  })
})

// ---------------Time Slot and Quantity Page--------------
// ---------------Place Order Page---------------------
router.post('/placeorder-new', function (req, res) {
  console.log(req.body.phoneno)
  // var phone = req.body.phoneno != null ? req.body.phoneno: null;
  var timeSlotId = req.body.timeSlot_id
  var addressId = req.body.address_id
  var quantity = req.body.quantity
  var phone = req.body.phoneno
  var result = {}
  console.log(phone + ' ' + timeSlotId + ' ' + addressId + ' ' + quantity)
  placeOrder(phone, timeSlotId, addressId, quantity, function (err, resp) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
      }
      // logger.error(err);
      res.send(result)
      throw err
    }
    // get the Primary key/Auto Generated column during the insert command
    result = {
      'output': 'Success',
      'error': 'none',
      'orderid': resp.insertId
    }
    res.send(result)
    /* connection.query('update `drop`.order set sellerid = ? where orderid = ?;', ["ela123", resultid], function (err) {
              if (err) console.log(err);
              else
                console.log(resultid);
            }); */
  })
})

// ------------Registration Page Starts---------------

router.post('/register', function (req, res) {
  var phone = req.body.phoneno
  var password = req.body.password
  var addressId = req.body.address_id
  var username = req.body.username

  genSalt(10, function (err, salt) {
    if (err) throw err
    _hash(password, salt, null, function (err, hash) { // null - for progress
      console.log('Hash - ' + hash + ' ' + salt + ' ' + hash.length + ' - ' + salt.length)
      if (err) {
        var result = {
          'output': 'error',
          'error': 'unknown'
        }
        // logger.error(err);
        res.send(result)
        throw err
      } else {
        insertPassword(hash, function (err, passwordResp) {
          if (err) {
            result = {
              'output': 'error',
              'error': 'unknown'
            }
            // logger.error(err);
            res.send(result)
            throw err
          } else {
            addNewUser(passwordResp.insertId, username, phone, addressId, function (err, userResp) {
              if (err) {
                result = {
                  'output': 'error',
                  'error': 'unknown'
                }
                //   logger.error(err);
                res.send(result)
                throw err
              } else {
                result = {
                  'output': 'Success',
                  'error': 'none'

                }
                res.send(result)
              }
            })
          }
        })
      }
    })
  })

  console.log(req.body)
})

export default router
