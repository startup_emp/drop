// Hema code starts
import { compare } from 'bcrypt-nodejs'
import { sign } from 'jsonwebtoken'
import { secretkey as key } from '../../constants'
// Hema code ends

import { login, getPassword } from '../common/query'
import { Router } from 'express'
var router = Router()

/** Check if the password corrosponding to the phone number matches in the db
 *Req=>/login
 *=> Body Form-URL-Encoded => phoneno:7418209937, password:elan
 *Res=>err || "error" || the password corrosponding to the phone number || "Wrong Password"
 */
router.post('/login', function (req, res) {
  var phone = req.body.phoneno
  var password = req.body.password
  var result = {}
  login(phone, function (err, rows) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
      }
      // logger.error(err);
      res.send(result)
      throw err
    } else
    if (rows.length <= 0) {
      result = {
        'output': 'error',
        'error': 'user_not_found'
      }
      res.send(result)
    } else {
      var passwordId = rows[0].password_id
      getPassword(passwordId, function (err, rows) {
        if (err) {
          result = {
            'output': 'error',
            'error': 'unknown'
          }
          // logger.error(err);
          res.send(result)
          throw err
        }
        compare(password, rows[0].password, function (err, out) {
          if (err) {
            result = {
              'output': 'error',
              'error': 'unknown'
            }
            // logger.error(err);
            res.send(result)
            throw err
          } else
          if (out) {
            var token = sign({
              userid: phone
            }, key)
            result = {
              'output': 'true',
              'bearer': token
            }
            res.send(result)
          } else {
            result = {
              'output': 'error',
              'error': 'wrong_password'
            }
            res.send(result)
          }
        })
      })
    }
  })
})
export default router
