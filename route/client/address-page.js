import { Router } from 'express'
var router = Router()

/** Get all the address related to a particular phone number
 *Req=>/address?phoneno=7418209937
 *Res=>err || "error" || all the address corrosponding to a phone number
 */

router.get('/address', function (req, res) {
  var phone = req.query.phoneno
  var address = []
  var result = {}

  connection.query('', phone, function (err, rows, fields) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
      }
      res.send(result)
      throw err
    } else
    if (rows.length <= 0) {
      result = {
        'output': 'error'
      }
      res.send(result)
    } else {
      for (let i = 1; i <= 5; i++) {
        const address_str = 'address_id_' + i.toString()
        connection.query('', rows[0][address_str], function (err, row_s, fields) {
          if (err) {
            result = {
              'output': 'error',
              'error': 'unknown'
            }
            res.send(result)
            throw err
          }
          var pincode_id = row_s.length == 0 ? 0 : row_s[0].pincode_id
          connection.query('', pincode_id, function (err, row_t, fields) {
            if (err) {
              result = {
                'output': 'error',
                'error': 'unknown'
              }
              res.send(result)
              throw err
            } else
            if (row_t.length != 0) {
              row_s[0].pincode = row_t[0].pincode
              address.push(row_s[0])
            }
            if (i === 5) { res.send(address) }
          })
        })
      }
    }
  })
})

/** Add a new address to a particular phone number
 *Req=>/address
 *   =>Body Form-URL-Encoded => phoneno:7418209937, address:asf-8848-asd-asd-mana-sdfgsd-1-asdf
 *Res=>err || "error" || all the address corrosponding to a phone number
 */
router.post('/address', function (req, res) {
  var phone = req.body.phoneno
  var address = req.body.address
  var address_str = null
  var address_id = null
  var result = {}
  console.log(phone + '\n' + address)
  var arr = address.split('**')
  query_str = 'CALL `USP_AddNewAddress`(?, ?, ?, ?, ?, ?, ?, ?,?,@output_param);SELECT @output_param'
  connection.query(query_str, [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], phone], function (err, rows, fields) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
      }
    
      res.send(result)
      throw err
    }
    // console.log(rows[1][0]['@output_param'].toString());
    else {
      result = {
        'output': 'Success',
        'error': 'none',
        'InsertedRowId': rows[1][0]['@output_param'].toString()
      }
      console.log(rows[1][0]['@output_param'].toString())
      res.send(result)
    }
  })
})

/** Update the address of a particular phone number
 *Req=>/address/10
 *   =>/Body Form-URL-Encoded => phoneno:7418209937, address:asf-888-asd-asd-mana-sdfgsd-1-asdf
 *Res=>err || "error" || all the address corrosponding to a phone number
 */
router.patch('/address/:address_id', function (req, res) {
  var address_id = req.params.address_id
  // var phone = req.body.phoneno;
  var address = req.body.address
  var arr = address.split('**')
  connection.query('update drop.address_details  set name = ?, phone = ?, door_no = ?, street = ?, area = ?, town_city = ?, pincode_id = ?, landmark = ? where address_id = ?', [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], address_id], function (err, result) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
      }
      res.send(result)
      throw err
    } else
    if (result.affectedRows <= 0) {
      result = {
        'output': 'No rows updated',
        'error': 'none',
        'affectedRows': result.affectedRows.toString()
      }
      res.send(result)
    } else {
      result = {
        'output': 'Success',
        'error': 'none',
        'affectedRows': result.affectedRows.toString()
      }
      res.send(result)
    }
  })
})

/** Delete the address of a particular phone number
 *Req=>/address/10
 *   =>/Body Form-URL-Encoded => phoneno:7418209937, address:asf-888-asd-asd-mana-sdfgsd-1-asdf
 *Res=>err || "error" || all the address corrosponding to a phone number
 */
router.delete('/address/:address_id', function (req, res) {
  var address_id = req.params.address_id

  var address_str = null
  console.log(phone + address_id)
  connection.query('', phone, function (err, rows, fields) {
    if (err) {
      result = {
        'output': 'error',
        'error': 'unknown'
      }
      res.send(result)
      throw err
    }
    switch (address_id.toString()) {
      case (rows[0].address_id_1 != null ? rows[0].address_id_1.toString() : null):
        address_str = 'address_id_1'
        break
      case (rows[0].address_id_2 != null ? rows[0].address_id_2.toString() : null):
        address_str = 'address_id_2'
        break
      case (rows[0].address_id_3 != null ? rows[0].address_id_3.toString() : null):
        address_str = 'address_id_3'
        break
      case (rows[0].address_id_4 != null ? rows[0].address_id_4.toString() : null):
        address_str = 'address_id_4'
        break
      case (rows[0].address_id_5 != null ? rows[0].address_id_5.toString() : null):
        address_str = 'address_id_5'
        break
    }
    console.log(address_str)
    if (address_str != null) {
      connection.query('', function (err, result) {
        if (err) {
          result = {
            'output': 'error',
            'error': 'unknown'
          }
          res.send(result)
          throw err
        } else
        if (result.affectedRows != 0) {
          connection.query('', address_id, function (err, result) {
            if (err) throw err
            result = {
              'output': 'Success',
              'error': 'none',
              'affectedRows': result.affectedRows.toString()
            }
            res.send(result)
          })
        }
      })
    }
  })
})

export default router
