import { resolve } from 'path'
import { loginCheck } from '../common/query'
import { Router } from 'express'
var router = Router()

/** Check if the Phone Number received from the client app is registered
 *Get all the Pincode and corrosponding Area available for the app */
router.get('/login-check', function (req, res) {
  var result = {}
  var phone = req.query.phoneno
  if (phone) {
    /** Check if the Phone Number received from the client app is registered
         *Req=>/login-check?phoneno=7418209937
         *Res=>err || "error" || the phone number sent in the req
         */
    if (/^\d+$/.test(phone)) {
      loginCheck(phone, function (err, rows) {
        if (err) {
          result = {
            'output': 'error',
            'error': 'unknown'
          }
          // logger.error(err);
          res.send(result)
          throw err
        } else
        if (rows.length <= 0) {
          result = {
            'output': 'error',
            'error': 'new_user'
          }
        } else {
          result = {
            'output': 'true'
          }
        }
        res.send(result)
      })
    } else {
      result = {
        'output': 'error',
        'error': 'not_num_value'
      }
      // logger.info(result);
      res.send(result)
    }
  } else {
    /** Get all the Pincode and corrosponding Area available for the app
         *Req=>/login-check
         *Res=>all pincode with its corrosponding area name
         */
    res.sendFile(resolve('Res/pincode.json'))
    /* res.sendFile("Res/pincode.json", {
            root: __dirname
        }); */
  }
})
export default router
