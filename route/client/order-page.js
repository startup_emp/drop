import { Router } from 'express'
import { logger } from '../common/logger'
import { placeOrder } from '../common/query'
var router = Router()

router.post('/placeorder', function (req, res) {
  console.log(req.body.phoneno)
  // var phone = req.body.phoneno != null ? req.body.phoneno: null;
  var timeSlotId = req.body.timeSlot_id
  var addressId = req.body.address_id
  var quantity = req.body.quantity
  var phoneno = req.body.phoneno
  placeOrder(phoneno, timeSlotId, addressId, quantity, function (err, res) {
    if (err) {
      var result = {
        'output': 'error',
        'error': 'unknown'
      }
      logger.error(err)
      res.send(result)
      throw err
    }
    // get the Primary key/Auto Generated column during the insert command
    result = {
      'output': 'Success',
      'error': 'none',
      'orderid': res.insertId
    }
    res.send(result)
    /* connection.query('update `drop`.order set sellerid = ? where orderid = ?;', ["ela123", resultid], function (err) {
          if (err) console.log(err);
          else
            console.log(resultid);
        }); */
  })
})

export default router
