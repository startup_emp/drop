var winston = require('winston');
const { createLogger, format, transports } = require('winston');//Hema Code change
const { combine, timestamp, label, printf } = format; //Hema Code change
function logging() {     

     const myFormat = printf(info => {
      return `${info.timestamp} [${info.label}] ${info.level}: ${info.message.json()}`;
    });
  
    logger = createLogger({
      format: combine(
        label({ label: 'right meow!' }),
        timestamp(),
        format.json(),
        myFormat
      ),
      transports: [new winston.transports.File({
        filename: 'combined.log',
        timestamp: false
      })]
    }); 
    const tsFormat = function () {
        return (new Date()).toLocaleTimeString();
    };

    //
    // If we're not in production then log to the `console` with the format:
    // `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
    // 
    if (process.env.NODE_ENV !== 'production') {
        logger.add(new winston.transports.Console({
            format: winston.format.simple()
        }));
    }
}

module.exports.logger = winston.createLogger({ //Hema Code change
    level: 'info',
    format: combine(
        timestamp(),
        format.json()
    ),
    transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log` 
        // - Write all logs error (and below) to `error.log`.
        //
        new winston.transports.File({
            filename: 'error.log',
            level: 'error',
        }),
        new winston.transports.File({
            filename: 'combined.log',
            timestamp: false
        }),
        new winston.transports.Console()
    ]
});