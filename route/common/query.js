import { createPool } from 'mysql'

// Db configuration
import { dbConfig } from './config'

var pool = createPool({
  host: dbConfig.dbEndpoint,
  user: dbConfig.userName,
  password: dbConfig.password,
  database: dbConfig.dbName,
  connectionLimit: 10,
  supportBigNumbers: true
})

// first page
export function loginCheck (phone, callback) {
  var sql = 'select user_id from user_details where phone = ?'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [phone], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(null, results)
    })
  })
}

// login page
export function login (phone, callback) {
  var sql = 'select password_id from user_details where phone = ?'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [phone], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(null, results)
    })
  })
}

export function getPassword (passwordId, callback) {
  var sql = 'select password from password_details where password_id = ?'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [passwordId], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(null, results)
    })
  })
}

export function insertPassword (hash, callback) {
  var sql = 'insert into password_details(password) values(?)'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, hash, function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(null, results)
    })
  })
}

// functions for new user
export function addAddress (arr, callback) {
  var sql = 'insert into address_details(name, phone, door_no, street, area, town_city, pincode_id, landmark) values(?, ?, ?, ?, ?, ?, ?, ?)'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7]], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(null, results)
    })
  })
}

export function updateAddress (arr, addressId, callback) {
  var sql = 'update address_details  set name = ?, phone = ?, door_no = ?, street = ?, area = ?, town_city = ?, pincode_id = ?, landmark = ? where address_id = ?'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], addressId], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(null, results)
    })
  })
}

export function deleteAddress (addressId, callback) {
  var sql = 'delete from address_details where address_id = ?'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, addressId, function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(null, results)
    })
  })
}

export function placeOrder (phone, timeSlotId, addressId, quantity, callback) {
  var sql = 'insert into order_details(phone, timeslot_id, address_id, quantity) values(?, ?, ?, ?)'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [phone, timeSlotId, addressId, quantity], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(null, results)
    })
  })
}

export function addNewUser (insertId, username, phone, addressId, callback) {
  var sql = 'insert into user_details( def_timeslot, password_id,user_name,phone,address_id_1) values(?,?,?,?,?)'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [1, insertId, username, phone, addressId], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(null, results)
    })
  })
}

export function getUserAddresses (phone, callback) {
  var sql = 'select address_id_1, address_id_2, address_id_3, address_id_4, address_id_5 from user_details where phone = ?'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, phone, function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(null, results)
    })
  })
}

export function deleteUserAddresse (addressNo, addressId, callback) {
  var sql = 'update user_details set ?? = NULL where ?? = ?'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, [addressNo, addressNo, addressId], function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(null, results)
    })
  })
}

export function getAddressDetails (addressId, callback) {
  var sql = 'select * from address_details where address_id = ?'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, addressId, function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(null, results)
    })
  })
}

export function getPincodeDetails (pincodeId, callback) {
  var sql = 'select pincode from pincode_details where pincode_id = ?'
  // get a connection from the pool
  pool.getConnection(function (err, connection) {
    if (err) {
      console.log(err)
      callback(new Error('error'))
      return
    }
    // make the query
    connection.query(sql, pincodeId, function (err, results) {
      connection.release()
      if (err) {
        console.log(err)
        callback(new Error('error'))
        return
      }
      callback(null, results)
    })
  })
}
