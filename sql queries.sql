insert into `drop`.order(phone, timeslot, sellerid) values("3454434354", "2-6", "ela123");
SELECT LAST_INSERT_ID();

//Scheduling an event every day at a given time
CREATE EVENT my_event
  ON SCHEDULE
    EVERY 1 DAY
    STARTS '2014-04-30 00:20:00' ON COMPLETION PRESERVE ENABLE 
  DO
    # My query

CREATE USER 'application'@'%' IDENTIFIED BY 'app1234@';
GRANT SELECT, INSERT, UPDATE, DELETE, SHOW DATABASES, CREATE TEMPORARY TABLES, EXECUTE, REPLICATION SLAVE, REPLICATION CLIENT, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, TRIGGER ON *.* TO 'application'@'%' WITH GRANT OPTION;
REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'application'@'%';

//Hema SP
DROP PROCEDURE IF EXISTS `USP_AddNewAddress`;
DELIMITER //
CREATE DEFINER=`dropDB`@`%` PROCEDURE `USP_AddNewAddress`(
	IN `Param_Name` VARCHAR(45),
	IN `Param_AdrPhoneNo` BIGINT,
	IN `Param_DoorNo` VARCHAR(45),
	IN `Param_Street` VARCHAR(45),
	IN `Param_Area` VARCHAR(245),
	IN `Param_TownCity` VARCHAR(45),
	IN `Param_Pincode_id` MEDIUMINT,
	IN `Param_Landmark` VARCHAR(45),
	IN `Param_PrimaryPhone` BIGINT,
	OUT `output_param` BIGINT
)
    DETERMINISTIC
    COMMENT 'To add new address'
BEGIN
DECLARE addressId varchar(20);
DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
BEGIN
    ROLLBACK;
END;
START TRANSACTION;

insert into drop.address_details(name, phone, door_no, street, area, town_city, pincode_id, landmark)
values (Param_Name,Param_AdrPhoneNo,Param_DoorNo,Param_Street,Param_Area,Param_TownCity,Param_Pincode_id,Param_Landmark);
  SET output_param = LAST_INSERT_ID();

set addressId=(select address_id from drop.address_details order by address_id desc limit 1);

   IF (select address_id_1 from drop.user_details where phone = Param_PrimaryPhone) IS NULL THEN
     update drop.user_details set address_id_1 =addressId  where phone = Param_PrimaryPhone;

   ELSEIF (select address_id_2 from drop.user_details where phone = Param_PrimaryPhone) IS NULL  THEN
   update drop.user_details set address_id_2 =addressId  where phone = Param_PrimaryPhone;


   ELSEIF (select address_id_3 from drop.user_details where phone = Param_PrimaryPhone) IS NULL  THEN
   update drop.user_details set address_id_3 =addressId  where phone = Param_PrimaryPhone;

     
      ELSEIF (select address_id_4 from drop.user_details where phone = Param_PrimaryPhone) IS NULL  THEN
    update drop.user_details set address_id_4 =addressId  where phone = Param_PrimaryPhone;

     
      ELSEIF (select address_id_5 from drop.user_details where phone = Param_PrimaryPhone) IS NULL  THEN
  update drop.user_details set address_id_5 =addressId  where phone = Param_PrimaryPhone;


   END IF;
  
   COMMIT;
   

   END//
DELIMITER ;

DROP PROCEDURE IF EXISTS `USP_Order_History`;
DELIMITER //
CREATE DEFINER=`drop`@`%` PROCEDURE `USP_Order_History`(
	IN `param_Phone` BIGINT
)
    READS SQL DATA
    COMMENT 'To Fetch the order history'
BEGIN
 SELECT order_details.order_id,timeslots.timeslot,address_details.name
  FROM  order_details
  JOIN (SELECT timeslots.timeslot,timeslots.timeslot_id              
          FROM  timeslots
        ) timeslots ON timeslots.timeslot_id = order_details.timeslot_id
        
          JOIN (SELECT address_details.name,address_details.address_id              
          FROM  address_details
        ) address_details ON address_details.address_id = order_details.address_id     
        
        where order_details.phone=param_Phone;
END//
DELIMITER ;